Info: Android app that displays a list of technology related news

Used libraries:

- Coil 
- Moshi 
- Retrofit 
- AdapterDelegates 
- Coroutines 
- Hilt 
- Timber 
- JUnit 
- Mockk 
- Espresso


How to build and run app

Set your _sdk.dir_ and _NEWSAPI_API_KEY_ in <project_directory>\local.properties eg.:
```
sdk.dir=C:\Users\John\AppData\Local\Android\Sdk 
NEWSAPI_API_KEY=xyz
```


Execute _./gradlew assemble_ release while in <project_directory>

Install APK found under <project_directory>\app\build\outputs\apk\release

Run unit tests: _./gradlew test_

Run instrumentation tests: _./gradlew connectedAndroidTest_ (tested on Pixel 2 emulator, API 30)

Download prebuilt binaries: https://gitlab.com/dnc/newsfort/-/releases
