package com.example.newsfort.domain.news.article.repositories

import com.example.newsfort.domain.news.article.models.NewsArticle

interface NewsArticleRepository {
    suspend fun getNewsArticles(): List<NewsArticle>
}