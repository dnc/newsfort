package com.example.newsfort.domain.user.preferences.repositories

import com.example.newsfort.domain.news.article.models.NewsArticle

interface UserPreferencesRepository {
    /**
     * Returns the user selected [news article group][NewsArticle.Group]
     */
    suspend fun getSelectedNewsArticleGroup(): NewsArticle.Group
}