package com.example.newsfort.domain.news.article.models

import java.time.LocalDateTime

data class NewsArticle(
    val group: Group,
    val title: String,
    val content: String,
    val illustrationUrl: String,
    val publicationDate: LocalDateTime
) {
    data class Group(
        val country: String,
        val category: String
    )
}