package com.example.newsfort.data.di

import com.example.newsfort.data.news.article.repositories.NewsArticleMapper
import com.example.newsfort.data.news.article.repositories.NewsArticleMapperImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class MapperModule {
    @Binds
    internal abstract fun bindNewsArticleMapper(mapper: NewsArticleMapperImpl): NewsArticleMapper
}
