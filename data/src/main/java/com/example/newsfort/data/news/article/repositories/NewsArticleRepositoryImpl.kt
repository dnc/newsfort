package com.example.newsfort.data.news.article.repositories

import com.example.newsfort.data.news.article.services.NewsArticleService
import com.example.newsfort.domain.news.article.models.NewsArticle
import com.example.newsfort.domain.news.article.repositories.NewsArticleRepository
import com.example.newsfort.domain.user.preferences.repositories.UserPreferencesRepository
import javax.inject.Inject

// TODO: write tests
internal class NewsArticleRepositoryImpl @Inject constructor(
    private val newsArticleService: NewsArticleService,
    private val newsArticleMapper: NewsArticleMapper,
    private val userPreferencesRepository: UserPreferencesRepository
) : NewsArticleRepository {
    override suspend fun getNewsArticles(): List<NewsArticle> {
        // TODO: error handling
        val newsArticleGroup = userPreferencesRepository.getSelectedNewsArticleGroup()
        return newsArticleService.getNewsArticles(country = newsArticleGroup.country, category = newsArticleGroup.category).run {
            newsArticleMapper.invoke(this)
        }
    }
}