package com.example.newsfort.data.user.preferences.repositories

import com.example.newsfort.domain.news.article.models.NewsArticle
import com.example.newsfort.domain.user.preferences.repositories.UserPreferencesRepository
import javax.inject.Inject

// TODO: write tests
internal class UserPreferencesRepositoryImpl @Inject constructor() : UserPreferencesRepository {
    override suspend fun getSelectedNewsArticleGroup(): NewsArticle.Group {
        // TODO: implement setter & selection screen, and return values from shared preferences
        return NewsArticle.Group("us", "technology")
    }
}