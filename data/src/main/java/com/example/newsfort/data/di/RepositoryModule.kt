package com.example.newsfort.data.di

import com.example.newsfort.data.news.article.repositories.NewsArticleRepositoryImpl
import com.example.newsfort.data.user.preferences.repositories.UserPreferencesRepositoryImpl
import com.example.newsfort.domain.news.article.repositories.NewsArticleRepository
import com.example.newsfort.domain.user.preferences.repositories.UserPreferencesRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class RepositoryModule {
    @Binds
    internal abstract fun bindUserPreferencesRepository(repo: UserPreferencesRepositoryImpl): UserPreferencesRepository

    @Binds
    internal abstract fun bindNewsArticleRepository(repo: NewsArticleRepositoryImpl): NewsArticleRepository
}