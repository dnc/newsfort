package com.example.newsfort.data.news.article.repositories

import com.example.newsfort.data.news.article.services.NewsArticleService
import com.squareup.moshi.Json
import java.time.LocalDateTime

internal data class NewsArticleDto(
    @Json(name = "title") val title: String,
    @Json(name = "publishedAt") private val rawPublicationDate: String,
    @Json(name = "content") val content: String?,
    @Json(name = "urlToImage") val illustrationUrl: String?
) {
    val publicationDate: LocalDateTime = LocalDateTime.parse(rawPublicationDate, NewsArticleService.dateTimeFormatter)
}
