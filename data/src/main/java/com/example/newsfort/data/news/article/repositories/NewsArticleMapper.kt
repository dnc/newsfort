package com.example.newsfort.data.news.article.repositories

import com.example.newsfort.domain.news.article.models.NewsArticle
import com.example.newsfort.domain.user.preferences.repositories.UserPreferencesRepository
import javax.inject.Inject

internal interface NewsArticleMapper {
    suspend fun invoke(newsArticleResponseDto: NewsArticleResponseDto): List<NewsArticle>
}

// TODO: write tests
internal class NewsArticleMapperImpl @Inject constructor(
    private val userPreferencesRepository: UserPreferencesRepository
) : NewsArticleMapper {
    override suspend fun invoke(newsArticleResponseDto: NewsArticleResponseDto): List<NewsArticle> {
        val newsArticleGroup = userPreferencesRepository.getSelectedNewsArticleGroup()
        return newsArticleResponseDto.newsArticles.map {
            NewsArticle(
                group = newsArticleGroup,
                title = it.title,
                content = it.content.orEmpty(),
                illustrationUrl = it.illustrationUrl.orEmpty(),
                publicationDate = it.publicationDate
            )
        }
    }
}