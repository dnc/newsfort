package com.example.newsfort.data.di

import com.example.newsfort.data.news.article.services.NewsArticleService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import retrofit2.Retrofit
import retrofit2.create

@Module
@InstallIn(ActivityRetainedComponent::class)
object RetrofitServiceModule {
    @Provides
    internal fun provideNewsArticleService(retrofit: Retrofit): NewsArticleService = retrofit.create()
}
