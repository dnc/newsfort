package com.example.newsfort.data.news.article.services

import com.example.newsfort.data.news.article.repositories.NewsArticleResponseDto
import retrofit2.http.GET
import retrofit2.http.Query
import java.time.format.DateTimeFormatter

internal interface NewsArticleService {
    @GET("top-headlines")
    suspend fun getNewsArticles(
        @Query("country") country: String,
        @Query("category") category: String
    ): NewsArticleResponseDto

    companion object {
        val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'")
    }
}