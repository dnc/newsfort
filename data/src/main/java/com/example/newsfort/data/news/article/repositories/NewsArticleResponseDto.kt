package com.example.newsfort.data.news.article.repositories

import com.squareup.moshi.Json

internal data class NewsArticleResponseDto(
    @Json(name = "articles") val newsArticles: List<NewsArticleDto>
)
