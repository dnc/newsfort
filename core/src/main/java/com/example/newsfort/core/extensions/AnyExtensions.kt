package com.example.newsfort.core.extensions

fun <T : Any> T?.requireNotNull(): T = requireNotNull(this)