package com.example.newsfort.core.extensions

import io.mockk.mockk
import org.junit.Test

class AnyExtensionsKtTest {
    @Test
    fun `given non null value when requireNotNull invoked then returns value`() {
        val expectedValue = mockk<Any>()

        val actualValue = expectedValue.requireNotNull()

        assert(expectedValue == actualValue)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `given null value when requireNotNull invoked then throws exception`() {
        val expectedValue: Any? = null

        expectedValue.requireNotNull()
    }
}