package com.example.newsfort.ui.news.article.common

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.newsfort.domain.news.article.models.NewsArticle
import com.example.newsfort.domain.news.article.repositories.NewsArticleRepository
import com.example.newsfort.ui.news.article.details.model.NewsArticleDetailsModel
import com.example.newsfort.ui.news.article.details.model.NewsArticleDetailsModelMapper
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModelMapper
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.time.LocalDateTime

class NewsArticleViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val newsArticleFake = NewsArticle(
        group = NewsArticle.Group(NEWS_ARTICLE_COUNTRY, NEWS_ARTICLE_CATEGORY),
        title = NEWS_ARTICLE_TITLE,
        content = NEWS_ARTICLE_CONTENT,
        publicationDate = LocalDateTime.now(),
        illustrationUrl = NEWS_ARTICLE_ILLUSTRATION_URL
    )
    private val newsArticlesFake = listOf(newsArticleFake)
    private val newsArticleModelFake = NewsArticleListModel.NewsArticleModel(
        title = NEWS_ARTICLE_TITLE,
        publicationTime = NEWS_ARTICLE_PUBLICATION_TIME,
        illustrationUrl = NEWS_ARTICLE_ILLUSTRATION_URL
    )
    private val newsArticleModelsFake = listOf(newsArticleModelFake)
    private val newsArticleDetailsModelFake = NewsArticleDetailsModel(
        title = NEWS_ARTICLE_TITLE,
        category = NEWS_ARTICLE_CATEGORY,
        country = NEWS_ARTICLE_COUNTRY,
        content = NEWS_ARTICLE_CONTENT,
        illustrationUrl = NEWS_ARTICLE_ILLUSTRATION_URL
    )
    private val newsArticleListModelMapperMock = mockk<NewsArticleListModelMapper> {
        every { getCachedNewsArticle(newsArticleModelFake) } returns newsArticleFake
        every { this@mockk.invoke(newsArticlesFake) } returns newsArticleModelsFake
    }
    private val newsArticleDetailsModelMapperMock = mockk<NewsArticleDetailsModelMapper> {
        every { this@mockk.invoke(newsArticleFake) } returns newsArticleDetailsModelFake
    }
    private val newsArticleRepositoryMock = mockk<NewsArticleRepository> {
        coEvery { getNewsArticles() } returns newsArticlesFake
    }

    private val viewModel = NewsArticleViewModel(
        newsArticleListModelMapper = newsArticleListModelMapperMock,
        newsArticleDetailsModelMapper = newsArticleDetailsModelMapperMock,
        newsArticleRepository = newsArticleRepositoryMock,
    )

    @Test
    fun `when onResume is invoked then maps via mapper`() {
        viewModel.onResume(mockk())

        verify { newsArticleListModelMapperMock.invoke(newsArticlesFake) }
    }

    @Test
    fun `when onResume is invoked then emits news article list models`() {
        viewModel.onResume(mockk())

        assert(viewModel.newsArticles.value?.peekContent() == newsArticleModelsFake)
    }

    @Test
    fun `given news article list model when onNewsArticleClicked is invoked then emits news article details request`() {
        viewModel.onNewsArticleClicked(newsArticleModelFake)

        assert(viewModel.newsArticleDetailsRequest.value?.peekContent() == Unit)
    }

    @Test
    fun `given news article list model when onNewsArticleClicked is invoked then maps via mapper`() {
        viewModel.onNewsArticleClicked(newsArticleModelFake)

        verify { newsArticleDetailsModelMapperMock.invoke(newsArticleFake) }
    }

    @Test
    fun `given news article list model when onNewsArticleClicked is invoked then sets selected news article`() {
        viewModel.onNewsArticleClicked(newsArticleModelFake)

        assert(viewModel.selectedNewsArticleDetailsModel.value?.peekContent() == newsArticleDetailsModelFake)
    }

    private companion object {
        const val NEWS_ARTICLE_TITLE = "news_article_title"
        const val NEWS_ARTICLE_COUNTRY = "news_article_country"
        const val NEWS_ARTICLE_CATEGORY = "news_article_category"
        const val NEWS_ARTICLE_CONTENT = "news_article_content"
        const val NEWS_ARTICLE_PUBLICATION_TIME = "news_article_publication_time"
        const val NEWS_ARTICLE_ILLUSTRATION_URL = "news_article_illustration_url"
    }
}