package com.example.newsfort.ui.news.article.list

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commitNow
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.MutableLiveData
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollToPosition
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.example.newsfort.R
import com.example.newsfort.ui.news.article.common.NewsArticleViewModel
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel
import com.example.newsfort.ui.utils.Event
import com.example.newsfort.ui.utils.launchFragmentInHiltContainer
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.hamcrest.Matchers.allOf
import org.junit.Test

class NewsArticleListFragmentTest {
    private val newsArticleDetailsRequestFake = MutableLiveData<Event<Unit>>()
    private val newsArticlesFake = MutableLiveData<Event<List<NewsArticleListModel>>>()
    private val viewModelMock = mockk<NewsArticleViewModel>(relaxed = true) {
        every { this@mockk.newsArticles } returns newsArticlesFake
        every { this@mockk.isLoading } returns MutableLiveData(false)
        every { this@mockk.newsArticleDetailsRequest } returns newsArticleDetailsRequestFake
    }
    private val newsArticleModelFake = NewsArticleListModel.NewsArticleModel(
        title = NEWS_ARTICLE_TITLE,
        publicationTime = NEWS_ARTICLE_PUBLICATION_TIME,
        illustrationUrl = NEWS_ARTICLE_ILLUSTRATION_URL
    )
    private val navControllerFake = TestNavHostController(ApplicationProvider.getApplicationContext())

    @Test
    fun givenNewsArticlesLoadedWhenResumedThenDisplaysNewsArticles() {
        // Given
        newsArticlesFake.postValue(Event(listOf(newsArticleModelFake)))
        lateinit var fragment: Fragment
        val scenario = launchFragmentInHiltContainer<NewsArticleListFragment>(themeResId = R.style.Theme_NewsFort_NoActionBar) {
            viewModel = viewModelMock
            fragment = this
        }

        // When
        scenario.moveFragmentToState(fragment, Lifecycle.State.RESUMED)

        // Then
        onView(withId(R.id.newsArticleList)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(R.id.textTitle)).check(matches(allOf(isDisplayed(), withText(NEWS_ARTICLE_TITLE))))
        onView(withId(R.id.textPublicationTime)).check(matches(allOf(isDisplayed(), withText(NEWS_ARTICLE_PUBLICATION_TIME))))
        // TODO: code doesn't assert image. Create mockable image coil wrapper, and use mockito / mock to verify iteraction
    }

    @Test
    fun givenNewsArticlesLoadedAndResumedWhenNewsArticleClickedThenDelegatesToViewModel() {
        // Given
        newsArticlesFake.postValue(Event(listOf(newsArticleModelFake)))
        lateinit var fragment: Fragment
        val scenario = launchFragmentInHiltContainer<NewsArticleListFragment>(themeResId = R.style.Theme_NewsFort_NoActionBar) {
            viewModel = viewModelMock
            fragment = this
        }
        scenario.moveFragmentToState(fragment, Lifecycle.State.RESUMED)

        // When
        onView(withId(R.id.newsArticleList)).perform(scrollToPosition<RecyclerView.ViewHolder>(0))
        onView(withId(R.id.itemRoot)).perform(click())

        // Then
        verify { viewModelMock.onNewsArticleClicked(newsArticleModelFake) }
    }

    @Test
    fun whenNewsArticleRequestEmittedThenNavigatesToNewsArticleDetails() {
        // Given, When
        newsArticleDetailsRequestFake.postValue(Event(Unit))
        lateinit var fragment: Fragment
        val scenario = launchFragmentInHiltContainer<NewsArticleListFragment>(themeResId = R.style.Theme_NewsFort_NoActionBar) {
            navControllerFake.setGraph(R.navigation.main_nav_graph)
            viewModel = viewModelMock
            fragment = this
        }
        scenario.moveFragmentToState(fragment, Lifecycle.State.STARTED)
        Navigation.setViewNavController(fragment.requireView(), navControllerFake)
        scenario.moveFragmentToState(fragment, Lifecycle.State.RESUMED)

        // Then
        assert(navControllerFake.currentDestination?.id == R.id.newsArticleDetailsFragment)
    }

    // TODO: test that loading overlay is shown when data is loading

    // TODO: test that error snackbar is shown when there's an error

    private fun ActivityScenario<out AppCompatActivity>.moveFragmentToState(fragment: Fragment, lifeCycleState: Lifecycle.State) {
        onActivity { it.supportFragmentManager.commitNow { setMaxLifecycle(fragment, lifeCycleState) } }
    }

    private companion object {
        const val NEWS_ARTICLE_TITLE = "news_article_title"
        const val NEWS_ARTICLE_PUBLICATION_TIME = "news_article_publication_time"
        const val NEWS_ARTICLE_ILLUSTRATION_URL = "news_article_illustration_url"
    }
}