package com.example.newsfort.ui.adapters

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load
import com.example.newsfort.R

@BindingAdapter(value = ["url"])
fun ImageView.loadImage(url: String) {
    load(url) {
        crossfade(true)
        error(R.drawable.ic_baseline_broken_image_100)
        placeholder(R.drawable.ic_baseline_hourglass_top_100)
    }
}