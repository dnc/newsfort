package com.example.newsfort.ui.extensions

import android.view.LayoutInflater
import android.view.View

val View.layoutInflater: LayoutInflater get() = LayoutInflater.from(context)

fun View.setVisibility(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}