package com.example.newsfort.ui.news.article.list.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfort.databinding.NewsArticleListNewsArticleIllustrationItemBinding
import com.example.newsfort.ui.adapters.loadImage
import com.example.newsfort.ui.extensions.layoutInflater
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel.NewsArticleIllustrationModel
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

class NewsArticleIllustrationDelegate(
    private val onClicked: (NewsArticleIllustrationModel) -> Unit
) : AdapterDelegate<List<NewsArticleListModel>>() {
    override fun isForViewType(items: List<NewsArticleListModel>, position: Int): Boolean = items[position] is NewsArticleIllustrationModel

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ViewHolder(NewsArticleListNewsArticleIllustrationItemBinding.inflate(parent.layoutInflater, parent, false))
    }

    override fun onBindViewHolder(
        items: List<NewsArticleListModel>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as NewsArticleIllustrationModel)
    }

    inner class ViewHolder(private val binding: NewsArticleListNewsArticleIllustrationItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: NewsArticleIllustrationModel) {
            binding.imageIllustration.loadImage(model.url)
            binding.root.setOnClickListener { onClicked(model) }
        }
    }
}