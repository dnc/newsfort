package com.example.newsfort.ui.extensions

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.TypedValue
import androidx.core.content.ContextCompat
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.shape.ShapeAppearanceModel

fun Context.themeAttributeToColor(themeAttributeId: Int, fallbackColor: Int = Color.WHITE): Int {
    val outValue = TypedValue()
    val resolved = theme.resolveAttribute(themeAttributeId, outValue, true)
    if (resolved) {
        return ContextCompat.getColor(this, outValue.resourceId)
    }
    return fallbackColor
}

fun Context.themeAttributeToDimen(themeAttributeId: Int, fallbackDimen: Int = 0): Int {
    val outValue = TypedValue()
    val resolved = theme.resolveAttribute(themeAttributeId, outValue, true)
    if (resolved) {
        return resources.getDimension(outValue.resourceId).toInt()
    }
    return fallbackDimen
}

fun Context.createCutBottomRightCornerMaterialShapeDrawable(): MaterialShapeDrawable {
    val shapeAppearanceModel = ShapeAppearanceModel.builder()
        .setBottomRightCorner(CornerFamily.CUT, themeAttributeToDimen(android.R.attr.actionBarSize) / 3f)
        .build()

    return MaterialShapeDrawable(shapeAppearanceModel).apply {
        tintList = ColorStateList.valueOf(themeAttributeToColor(com.google.android.material.R.attr.colorPrimary))
    }
}