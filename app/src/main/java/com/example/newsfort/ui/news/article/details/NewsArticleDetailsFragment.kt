package com.example.newsfort.ui.news.article.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.newsfort.core.extensions.requireNotNull
import com.example.newsfort.databinding.NewsArticleDetailsFragmentBinding
import com.example.newsfort.ui.extensions.observeValue
import com.example.newsfort.ui.news.article.common.NewsArticleViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class NewsArticleDetailsFragment : Fragment() {
    private var binding: NewsArticleDetailsFragmentBinding? = null

    @Inject
    lateinit var viewModel: NewsArticleViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = NewsArticleDetailsFragmentBinding.inflate(inflater, container, false).also {
            this@NewsArticleDetailsFragment.binding = it
        }

        binding.toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initObservers()
    }

    private fun initObservers() {
        viewModel.selectedNewsArticleDetailsModel.observeValue(viewLifecycleOwner) {
            binding.requireNotNull().model = it
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}