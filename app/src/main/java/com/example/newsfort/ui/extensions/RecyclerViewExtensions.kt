package com.example.newsfort.ui.extensions

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration


fun RecyclerView.addItemDivider() {
    val styleAttributes = context.obtainStyledAttributes(intArrayOf(android.R.attr.listDivider))
    addItemDecoration(DividerItemDecorator(
        dividerDrawable = styleAttributes.getDrawable(0) ?: ColorDrawable(Color.BLACK)
    ))
    styleAttributes.recycle()
}

class DividerItemDecorator(private val dividerDrawable: Drawable) : ItemDecoration() {
    override fun onDrawOver(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        for (i in 1 until parent.childCount) {
            val child = parent.getChildAt(i)
            val dividerTop = child.bottom + (child.layoutParams as RecyclerView.LayoutParams).bottomMargin
            val dividerBottom = dividerTop + dividerDrawable.intrinsicHeight

            with(dividerDrawable) {
                setBounds(0, dividerTop, parent.width, dividerBottom)
                draw(canvas)
            }
        }
    }
}