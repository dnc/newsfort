package com.example.newsfort.ui.news.article.list.delegates

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfort.databinding.NewsArticleListNewsArticleItemBinding
import com.example.newsfort.ui.adapters.loadImage
import com.example.newsfort.ui.extensions.layoutInflater
import com.example.newsfort.ui.extensions.setVisibility
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel.NewsArticleModel
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

class NewsArticleDelegate(private val onClicked: (NewsArticleModel) -> Unit) : AdapterDelegate<List<NewsArticleListModel>>() {
    override fun isForViewType(items: List<NewsArticleListModel>, position: Int): Boolean = items[position] is NewsArticleModel

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return ViewHolder(NewsArticleListNewsArticleItemBinding.inflate(parent.layoutInflater, parent, false))
    }

    override fun onBindViewHolder(
        items: List<NewsArticleListModel>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) {
        (holder as ViewHolder).bind(items[position] as NewsArticleModel)
    }

    inner class ViewHolder(private val binding: NewsArticleListNewsArticleItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(model: NewsArticleModel) {
            with(binding) {
                textTitle.text = model.title
                textPublicationTime.text = model.publicationTime
                imageIllustration.setVisibility(model.illustrationUrl != null)

                model.illustrationUrl?.let {
                    imageIllustration.loadImage(it)
                }

                root.setOnClickListener { onClicked(model) }
            }
        }
    }
}