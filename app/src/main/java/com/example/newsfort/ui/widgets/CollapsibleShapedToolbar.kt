package com.example.newsfort.ui.widgets

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.annotation.Px
import com.example.newsfort.R
import com.example.newsfort.ui.extensions.createCutBottomRightCornerMaterialShapeDrawable
import com.google.android.material.appbar.MaterialToolbar

/**
 * A [MaterialToolbar] that collapses to the left as the user scrolls down. As the toolbar is collapsed, the title becomes collapsed too and the
 * toolbar's bottom right corner shape is changed
 * Inspired by https://github.com/ologe/fortnightly/blob/b29f0aa845ee41b81bc01661a9ca739c6e940cda/app/src/main/java/dev/olog/fortnightly/presentation/widget/FortnightlyToolbar.kt
 */
class CollapsibleShapedToolbar(context: Context, attributeSet: AttributeSet) : MaterialToolbar(context, attributeSet) {
    private val interpolator by lazy { LinearInterpolator() }
    private val textCollapsedTitle by lazy { findViewById<View>(R.id.textCollapsedToolbarTitle) } // 'T'
    private val textTitle by lazy { findViewById<View>(R.id.textToolbarTitle) } // 'Toolbar title'

    private val scrollAmountLimit by lazy { height * 2f }
    private val maxRightMargin by lazy { right.toFloat() - textCollapsedTitle.right.toFloat() - textCollapsedTitle.paddingEnd }

    private val shapeDrawable by lazy {
        context.createCutBottomRightCornerMaterialShapeDrawable().apply { interpolation = 0f }
    }

    init {
        if (!isInEditMode) {
            background = shapeDrawable
        }
    }

    private var totalScrollAmount = 0f

    fun update(scrollAmount: Int) {
        totalScrollAmount += scrollAmount
        val coercedScrollAmount = totalScrollAmount.coerceIn(0f, scrollAmountLimit)

        val ratio = (coercedScrollAmount / scrollAmountLimit).let { if (it.isNaN()) 0f else it }
        shapeDrawable.interpolation = ratio
        textCollapsedTitle.alpha = interpolator.getInterpolation(ratio)

        val invertedRatio = 1f - ratio
        textTitle.alpha = interpolator.getInterpolation(invertedRatio)

        setRightMargin(value = (maxRightMargin * ratio).toInt())
    }

    private fun setRightMargin(@Px value: Int) {
        val params = this.layoutParams as? ViewGroup.MarginLayoutParams ?: return
        params.rightMargin = value
        layoutParams = params
    }
}