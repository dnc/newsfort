package com.example.newsfort.ui.adapters

import android.graphics.drawable.ColorDrawable
import androidx.databinding.BindingAdapter
import com.example.newsfort.ui.extensions.createCutBottomRightCornerMaterialShapeDrawable
import com.example.newsfort.ui.extensions.themeAttributeToColor
import com.google.android.material.appbar.MaterialToolbar

@BindingAdapter(value = ["cutBottomRightCorner"])
fun MaterialToolbar.setCutBottomRightCorner(cutBottomRightCorner: Boolean = true) {
    background = if (cutBottomRightCorner) {
        context.createCutBottomRightCornerMaterialShapeDrawable()
    } else {
        ColorDrawable(context.themeAttributeToColor(com.google.android.material.R.attr.colorPrimary))
    }
}