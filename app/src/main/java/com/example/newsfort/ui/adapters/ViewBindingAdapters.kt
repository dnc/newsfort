package com.example.newsfort.ui.adapters

import android.view.View
import androidx.databinding.BindingAdapter
import com.example.newsfort.ui.extensions.setVisibility

@BindingAdapter(value = ["isVisible"])
fun View.setVisibility(isVisible: Boolean) {
    setVisibility(isVisible)
}