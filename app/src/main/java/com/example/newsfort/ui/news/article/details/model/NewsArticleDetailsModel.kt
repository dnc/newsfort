package com.example.newsfort.ui.news.article.details.model

data class NewsArticleDetailsModel(
    val title: String,
    val category: String,
    val country: String,
    val content: String,
    val illustrationUrl: String
)
