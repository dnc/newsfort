package com.example.newsfort.ui.news.article.list

import androidx.recyclerview.widget.DiffUtil
import com.example.newsfort.ui.news.article.list.delegates.NewsArticleDelegate
import com.example.newsfort.ui.news.article.list.delegates.NewsArticleIllustrationDelegate
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class NewsArticleListAdapter(onClicked: (NewsArticleListModel) -> Unit) : ListDelegationAdapter<List<NewsArticleListModel>>() {
    init {
        with(delegatesManager) {
            addDelegate(NewsArticleIllustrationDelegate(onClicked))
            addDelegate(NewsArticleDelegate(onClicked))
        }
    }

    var adapterItems = listOf<NewsArticleListModel>()
        set(value) {
            val diffResult = DiffUtil.calculateDiff(DiffUtilCallback(field, value))
            field = value
            items = field
            diffResult.dispatchUpdatesTo(this)
        }

    private class DiffUtilCallback(
        private val oldList: List<NewsArticleListModel>,
        private val newList: List<NewsArticleListModel>
    ) : DiffUtil.Callback() {

        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }
}