package com.example.newsfort.ui.news.article.list.model

sealed interface NewsArticleListModel {
    data class NewsArticleIllustrationModel(val url: String) : NewsArticleListModel
    data class NewsArticleModel(val title: String, val publicationTime: String, val illustrationUrl: String?) : NewsArticleListModel
}