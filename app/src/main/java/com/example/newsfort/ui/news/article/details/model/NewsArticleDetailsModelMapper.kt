package com.example.newsfort.ui.news.article.details.model

import com.example.newsfort.domain.news.article.models.NewsArticle
import javax.inject.Inject

interface NewsArticleDetailsModelMapper {
    fun invoke(newsArticle: NewsArticle): NewsArticleDetailsModel
}

class NewsArticleDetailsModelMapperImpl @Inject constructor() : NewsArticleDetailsModelMapper {
    override fun invoke(newsArticle: NewsArticle): NewsArticleDetailsModel {
        return NewsArticleDetailsModel(
            title = newsArticle.title,
            category = newsArticle.group.category,
            country = newsArticle.group.country,
            content = newsArticle.content,
            illustrationUrl = newsArticle.illustrationUrl
        )
    }
}