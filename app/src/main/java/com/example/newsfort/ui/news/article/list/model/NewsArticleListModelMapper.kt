package com.example.newsfort.ui.news.article.list.model

import com.example.newsfort.domain.news.article.models.NewsArticle
import java.time.Duration
import java.time.LocalDateTime
import javax.inject.Inject

interface NewsArticleListModelMapper {
    fun invoke(newsArticles: List<NewsArticle>): List<NewsArticleListModel>

    fun getCachedNewsArticle(newsArticleListModel: NewsArticleListModel): NewsArticle?
}

class NewsArticleListModelMapperImpl @Inject constructor() : NewsArticleListModelMapper {
    private val cache = mutableMapOf<NewsArticleListModel, NewsArticle>()

    override fun invoke(newsArticles: List<NewsArticle>): List<NewsArticleListModel> {
        val mapped = mutableListOf<NewsArticleListModel>()

        newsArticles.forEachIndexed { index, newsArticle ->
            if (index == 0) {
                mapped.add(
                    NewsArticleListModel.NewsArticleIllustrationModel(url = newsArticle.illustrationUrl).also {
                        it.addToCache(newsArticle)
                    }
                )
                mapped.add(
                    newsArticle.toNewsArticleModel(omitIllustrationUrl = true).also {
                        it.addToCache(newsArticle)
                    }
                )
            } else {
                mapped.add(
                    newsArticle.toNewsArticleModel(omitIllustrationUrl = false).also {
                        it.addToCache(newsArticle)
                    }
                )
            }
        }

        return mapped
    }

    private fun NewsArticleListModel.addToCache(newsArticle: NewsArticle) {
        cache[this] = newsArticle
    }

    private fun NewsArticle.toNewsArticleModel(omitIllustrationUrl: Boolean): NewsArticleListModel.NewsArticleModel {
        return NewsArticleListModel.NewsArticleModel(
            title = title,
            publicationTime = publicationDate.toRelativeTimeString(),
            illustrationUrl = if (omitIllustrationUrl) null else illustrationUrl
        )
    }

    override fun getCachedNewsArticle(newsArticleListModel: NewsArticleListModel): NewsArticle? = cache[newsArticleListModel]

    private fun LocalDateTime.toRelativeTimeString(): String {
        val now = LocalDateTime.now()
        require(this < now) { "The specified time has to be in the past" }
        val durationInMinutes = Duration.between(this, now).toMinutes()

        // x>=0 mins -> 1m
        // x<=59 mins -> xm
        // x>=60 mins -> xH
        return when {
            durationInMinutes <= 59 -> "${durationInMinutes.coerceAtLeast(1)}m"
            else -> "${durationInMinutes / 60}H"
        }
    }
}