package com.example.newsfort.ui.news.article.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.newsfort.R
import com.example.newsfort.core.extensions.requireNotNull
import com.example.newsfort.databinding.NewsArticleListFragmentBinding
import com.example.newsfort.ui.extensions.addItemDivider
import com.example.newsfort.ui.extensions.observeValue
import com.example.newsfort.ui.news.article.common.NewsArticleViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class NewsArticleListFragment : Fragment() {
    private var binding: NewsArticleListFragmentBinding? = null

    @Inject
    lateinit var viewModel: NewsArticleViewModel

    private val newsArticleListAdapter by lazy { NewsArticleListAdapter(onClicked = viewModel::onNewsArticleClicked) }

    private var newsArticleListVerticalScrollAmount = 0

    private val listener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            binding.requireNotNull().toolbar.update(dy)
            newsArticleListVerticalScrollAmount += dy
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewLifecycleOwner.lifecycle.addObserver(viewModel)

        val binding = NewsArticleListFragmentBinding.inflate(inflater, container, false).also {
            this@NewsArticleListFragment.binding = it
            it.lifecycleOwner = viewLifecycleOwner
            it.viewModel = viewModel
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding.requireNotNull()) {
            toolbar.post { toolbar.update(newsArticleListVerticalScrollAmount) }
            newsArticleList.apply {
                adapter = newsArticleListAdapter
                addItemDivider()
            }
        }
    }

    private fun initObservers() {
        with(viewModel) {
            newsArticles.observeValue(viewLifecycleOwner) {
                newsArticleListAdapter.adapterItems = it
            }
            newsArticleDetailsRequest.observeValue(viewLifecycleOwner) {
                // An improvement would be to use Safe-Args: https://developer.android.com/guide/navigation/navigation-pass-data#Safe-args
                findNavController().navigate(R.id.newsArticleDetailsFragment)
            }
            isError.observeValue(viewLifecycleOwner) { isError ->
                if (isError) {
                    Snackbar.make(requireView(), R.string.request_error_message, Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()

        with(binding.requireNotNull()) {
            newsArticleList.addOnScrollListener(listener)
        }

        initObservers()
    }

    override fun onPause() {
        super.onPause()
        binding.requireNotNull().newsArticleList.removeOnScrollListener(listener)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}