package com.example.newsfort.ui.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.example.newsfort.ui.utils.Event

fun <T : Any?> LiveData<Event<T>>.observeValue(owner: LifecycleOwner, observer: (T) -> Unit) {
    observe(
        owner,
        {
            val value = it?.consume()

            if (value != null) {
                observer(value)
            }
        }
    )
}
