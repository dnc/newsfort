package com.example.newsfort.ui.news.article.common

import androidx.annotation.MainThread
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newsfort.domain.news.article.repositories.NewsArticleRepository
import com.example.newsfort.ui.news.article.details.model.NewsArticleDetailsModel
import com.example.newsfort.ui.news.article.details.model.NewsArticleDetailsModelMapper
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModel
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModelMapper
import com.example.newsfort.ui.utils.Event
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class NewsArticleViewModel(
    private val newsArticleListModelMapper: NewsArticleListModelMapper,
    private val newsArticleDetailsModelMapper: NewsArticleDetailsModelMapper,
    private val newsArticleRepository: NewsArticleRepository
) : ViewModel(), DefaultLifecycleObserver {
    private val _newsArticles = MutableLiveData<Event<List<NewsArticleListModel>>>()
    val newsArticles: LiveData<Event<List<NewsArticleListModel>>> = _newsArticles

    private val _newsArticleDetailsRequest = MutableLiveData<Event<Unit>>()
    val newsArticleDetailsRequest: LiveData<Event<Unit>> = _newsArticleDetailsRequest

    private val _selectedNewsArticleDetailsModel = MutableLiveData<Event<NewsArticleDetailsModel?>>()
    val selectedNewsArticleDetailsModel: LiveData<Event<NewsArticleDetailsModel?>> = _selectedNewsArticleDetailsModel

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _isError = MutableLiveData<Event<Boolean>>()
    val isError: LiveData<Event<Boolean>> = _isError

    private var newsArticleLoadingJob: Job? = null

    override fun onResume(owner: LifecycleOwner) {
        // TODO: implement better error handling, block user input white loading, implement paging

        _isLoading.value = true
        _isError.value = Event(false)
        newsArticleLoadingJob?.cancel()
        newsArticleLoadingJob = viewModelScope.launch {
            val newsArticles = try {
                newsArticleRepository.getNewsArticles()
            } catch (e: Exception) {
                _isError.value = Event(true)
                _isLoading.value = false
                return@launch
            }
            _newsArticles.value = Event(newsArticleListModelMapper.invoke(newsArticles.filter { it.illustrationUrl.isNotBlank() }))
            _isLoading.value = false
        }
    }

    @MainThread
    fun onNewsArticleClicked(newsArticleListModel: NewsArticleListModel) {
        _selectedNewsArticleDetailsModel.value = Event(
            newsArticleListModelMapper.getCachedNewsArticle(newsArticleListModel)?.let {
                newsArticleDetailsModelMapper.invoke(it)
            }
        )
        _newsArticleDetailsRequest.value = Event(Unit)
    }
}