package com.example.newsfort.di

import com.example.newsfort.domain.news.article.repositories.NewsArticleRepository
import com.example.newsfort.ui.news.article.common.NewsArticleViewModel
import com.example.newsfort.ui.news.article.details.model.NewsArticleDetailsModelMapper
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModelMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import dagger.hilt.android.scopes.ActivityRetainedScoped

@Module
@InstallIn(ActivityRetainedComponent::class)
object ViewModelModule {
    @Provides
    @ActivityRetainedScoped
    fun provideNewsArticleViewModel(
        newsArticleListModelMapper: NewsArticleListModelMapper,
        newsArticleDetailsModelMapper: NewsArticleDetailsModelMapper,
        newsArticleRepository: NewsArticleRepository
    ) = NewsArticleViewModel(newsArticleListModelMapper, newsArticleDetailsModelMapper, newsArticleRepository)
}