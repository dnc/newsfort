package com.example.newsfort.di

import com.example.newsfort.ui.news.article.details.model.NewsArticleDetailsModelMapper
import com.example.newsfort.ui.news.article.details.model.NewsArticleDetailsModelMapperImpl
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModelMapper
import com.example.newsfort.ui.news.article.list.model.NewsArticleListModelMapperImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent

@Module
@InstallIn(ActivityRetainedComponent::class)
interface ModelMapperModule {
    @Binds
    fun bindNewsArticleListModelMapperImpl(mapper: NewsArticleListModelMapperImpl): NewsArticleListModelMapper

    @Binds
    fun bindNewsArticleDetailsModelMapperImpl(mapper: NewsArticleDetailsModelMapperImpl): NewsArticleDetailsModelMapper
}